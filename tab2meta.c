#include <stdio.h>
#include <stdlib.h>

#include <unistd.h>
#include <linux/input.h>

// clang-format off
static const struct input_event
tab_up          = {.type = EV_KEY, .code = KEY_TAB,      .value = 0},
meta_up         = {.type = EV_KEY, .code = KEY_LEFTMETA, .value = 0},
tab_down        = {.type = EV_KEY, .code = KEY_TAB,      .value = 1},
meta_down       = {.type = EV_KEY, .code = KEY_LEFTMETA, .value = 1},
tab_repeat      = {.type = EV_KEY, .code = KEY_TAB,      .value = 2},
meta_repeat     = {.type = EV_KEY, .code = KEY_LEFTMETA, .value = 2},
syn             = {.type = EV_SYN, .code = SYN_REPORT,   .value = 0};
// clang-format on

int equal(const struct input_event *first, const struct input_event *second) {
    return first->type == second->type && first->code == second->code &&
           first->value == second->value;
}

int read_event(struct input_event *event) {
    return fread(event, sizeof(struct input_event), 1, stdin) == 1;
}

void write_event(const struct input_event *event) {
    if (fwrite(event, sizeof(struct input_event), 1, stdout) != 1)
        exit(EXIT_FAILURE);
}

int main(int argc, char *argv[]) {
    double sec = 0; //not really seconds but close enough
    if (argc > 2 || (argc == 2 && sscanf(argv[1], "%lf", &sec) != 1) || sec < 0) {
        printf("usage: %s [seconds]\n"
               "[seconds] - number of seconds after which pressed Tab "
               "starts to be automatically interpreted as Meta; "
               "expected to be a positive real number, if it's omited or 0, "
               "Tab will be interpreted as Meta only when used "
               "in combination with other keys\n", argv[0]);
        return EXIT_FAILURE;
    }
    const double LONG_PRESS = sec * 1000 / 35; //one EV_MSC event every ~35ms on my keyboard

    int tab_is_down = 0, tab_possible = 1, count = 0;
    struct input_event input;

    setbuf(stdin, NULL); setbuf(stdout, NULL);

    while (read_event(&input)) {
        if (input.type == EV_MSC && input.code == MSC_SCAN) {
            //detect long press by counting EV_MSC MSC_SCAN events
            if (LONG_PRESS && tab_is_down && tab_possible && ++count > LONG_PRESS) {
                tab_possible = 0;
                write_event(&meta_down);
                write_event(&syn);
                usleep(20000);
            }
            continue;
        }

        if (input.type != EV_KEY) {
            write_event(&input);
            continue;
        }

        if (tab_is_down) {
            if (equal(&input, &tab_down) || equal(&input, &tab_repeat))
                continue;

            if (equal(&input, &tab_up)) {           //Tab release
                if (tab_possible) {                 //it's Tab
                    write_event(&tab_down);
                    write_event(&syn);
                    usleep(20000);
                    write_event(&tab_up);
                } else {                            //it's Meta
                    write_event(&meta_up);
                }
                //reset
                tab_is_down = 0;
                tab_possible = 1;
                count = 0;
                continue;
            }

            if (tab_possible && input.value) { //key combination - it's Meta
                tab_possible = 0;
                write_event(&meta_down);
                write_event(&syn);
                usleep(20000);
            }
        } else if (equal(&input, &tab_down)) {
            tab_is_down = 1;
            continue;
        }

        write_event(&input);
    }
}
